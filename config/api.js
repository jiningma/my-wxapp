const API_BASE_URL = 'http://127.0.0.1:8000/api/';
const getUsersList = API_BASE_URL + "users/"
module.exports = {
    loginByWeixin: API_BASE_URL + "loginbyweixin/",
    getUsersList: getUsersList
};
